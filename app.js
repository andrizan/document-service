require('dotenv').config()

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const router = require('./router/router')

const app = express()
const PORT = process.env.PORT || 3000

try {
  mongoose.connect(process.env.MONGO_URI)
  console.log("database connected ");
} catch (e) {
  console.error(e);
  process.exit();
}

app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(router)

app.listen(PORT, () => {
  console.log('listening on *:' + PORT)
})
