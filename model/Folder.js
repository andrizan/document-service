const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  id: {
    type: String
  },
  name: {
    type: String
  },
  type: {
    type: String,
    default: `folder`
  },
  is_public: {
    type: Boolean,
    default: true
  },
  share: {
    type: Array
  },
  timestamps: {
    type: String
  },
  owner_id: {
    type: String
  },
  company_id: {
    type: String
  },
  content: {
    type: Object,
    default: {}
  },
}, {
  minimize: false,
  strict: true,
  versionKey: false,
});


module.exports = mongoose.model('folder', Schema)
