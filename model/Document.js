const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  id: {
    type: String
  },
  name: {
    type: String
  },
  type: {
    type: String
  },
  folder_id: {
    type: String
  },
  content: {
    type: Object,
  },
  timestamps: {
    type: Number
  },
  owner_id: {
    type: String
  },
  share: {
    type: Array
  },
  company_id: {
    type: String
  }
}, {
  minimize: false,
  strict: true,
  versionKey: false,
})

module.exports = mongoose.model('document', Schema)
