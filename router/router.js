const express = require('express');
const router = express.Router();

const FolderController = require('../controller/FolderController')
const DocumentController = require('../controller/DocumentController')
const auth = require('../middleware/auth')


// router.use(auth)
router.get('/document-service', FolderController.getAll)
router.post('/document-service/folder', FolderController.addFolder)
router.delete('/document-service/folder', FolderController.deleteFolder)

router.post('/document-service/document', DocumentController.addDocument)
router.delete('/document-service/document', DocumentController.deleteDocument)
router.get('/document-service/folder/:folder_id', DocumentController.getFileFolder)
router.get('/document-service/document/:document_id', DocumentController.getDetailDoc)

module.exports = router
