const jwt = require('jsonwebtoken')

function auth(req, res, next) {
  let rawToken = req.headers.authorization || ''
  let token = rawToken.replace(/Bearer /g, '')
  console.log(token, process.env.PRIVATE_KEY);
  try {
    let result = jwt.verify(token, process.env.PRIVATE_KEY)
    if (result) {
      req.user_id = result.user_id
      req.company_id = result.company_id
      next()
    } else {
      res.status(401).json({
        errors: true,
        message: 'Unauthorized'
      })
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: true,
      message: error
    })
  }
}

module.exports = auth
