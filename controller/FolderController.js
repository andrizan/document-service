const Folder = require('../model/Folder')
const Document = require('../model/Document')

class FolderController {
  static async getAll(req, res) {
    let document = await Document.find({}, { _id: 0 })
    let folder = await Folder.find({}, { _id: 0 })
    res.status(200).json({
      error: false,
      data: document.concat(folder)
    })
  }

  static async addFolder(req, res) {
    let id = req.body.id
    let name = req.body.name
    let timestamp = req.body.timestamp
    let owner_id = req.user_id
    let company_id = req.company_id
    try {
      const cek = await Folder.findOne({ id })
      if (cek) {
        let data = await Folder.findOneAndUpdate({ id: id }, { name, timestamp })
        res.status(200).json({
          error: false,
          message: 'folder updated',
          data: {
            id, name, type: data.type, content: data.content, timestamp, owner_id, company_id,
          }
        })
      } else {
        let folder = new Folder({
          id, name, timestamp, owner_id, company_id
        })
        let data = await folder.save()
        res.status(200).json({
          error: false,
          message: 'folder created',
          data: {
            id, name, type: data.type, content: data.content, timestamp, owner_id, company_id,
          }
        })
      }
    } catch (error) {
      res.status(500).json({
        errors: error
      })
    }
  }

  static async deleteFolder(req, res) {
    let id = req.body.id
    try {
      await Folder.findOneAndDelete({ id: id })
      res.status(200).json({
        error: false,
        message: 'Success delete folder'
      })
    } catch (error) {
      res.status(500).json({
        error: 'internal server error'
      })
    }
  }

}

module.exports = FolderController
