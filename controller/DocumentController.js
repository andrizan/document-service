const Document = require('../model/Document')

class DocumentController {
  static async getListFile(req, res) {
    let folder_id = req.params.folder_id
    try {
      const data = await Document.find({ folder_id }, { _id: 0 })
      res.status(200).json({
        error: false,
        data
      })
    } catch (error) {
      res.status(500).json({
        errors: error
      })
    }
  }

  static async addDocument(req, res) {
    let id = req.body.id
    let name = req.body.name
    let type = req.body.type
    let folder_id = req.body.folder_id
    let content = req.body.content
    let timestamp = req.body.timestamp
    let share = req.body.share
    let owner_id = req.body.owner_id
    let company_id = req.body.company_id
    try {
      const cek = await Document.findOne({ id })
      if (cek) {
        const insert = {
          name, type, folder_id, content, timestamp, share, owner_id, company_id
        }
        const document = await Document.findOneAndUpdate({ id: id }, insert)
        res.status(200).json({
          error: false,
          message: 'success update document',
          data: {
            document: {
              id, name, type, folder_id, content, timestamp, owner_id, share,
            }
          }
        })
      } else {
        const insert = new Document({
          id, name, type, folder_id, content, timestamp, share, owner_id, company_id
        })
        const document = await insert.save()
        res.status(200).json({
          error: false,
          message: 'success set document',
          data: {
            document: {
              id, name, type, folder_id, content, timestamp, owner_id, share,
            }
          }
        })
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        errors: error
      })
    }
  }

  static async deleteDocument(req, res) {
    let id = req.body.id
    try {
      await Document.findOneAndDelete({ id: id })
      res.status(200).json({
        error: false,
        message: 'Success delete folder'
      })
    } catch (error) {
      res.status(500).json({
        error: 'internal server error'
      })
    }
  }

  static async getFileFolder(req, res) {
    let folder_id = req.params.folder_id
    try {
      const data = await Document.find({ folder_id }, { _id: 0 })
      res.status(200).json({
        error: false,
        data: data
      })
    } catch (error) {
      res.status(500).json({
        errors: error
      })
    }
  }

  static async getDetailDoc(req, res) {
    let document_id = req.params.document_id
    try {
      const data = await Document.findOne({ id: document_id }, { _id: 0 })
      res.status(200).json({
        error: false,
        data: {
          document: data
        }
      })
    } catch (error) {
      res.status(500).json({
        errors: error
      })
    }
  }

}

module.exports = DocumentController
